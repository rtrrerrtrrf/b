package th.rosenheim.oop;

// Class representing a small town, extending the Stadt class.
public class Kleinstadt extends Stadt {

    // Constructor for Kleinstadt, calls the parent constructor.
    public Kleinstadt(String name, Wetter wetter) {
        super(name, wetter);
    }

    // Returns the content for the small town's webpage.
    @Override
    public String getContent() {
        return "<p>In " + name + " ist es " + wetter.getWetter() + ".</p>";
    }

    // Returns the URL for the small town's webpage.
    @Override
    public String getURL() {
        return "wetter_kleinstadt_" + formatNameForURL() + ".html";
    }
}
