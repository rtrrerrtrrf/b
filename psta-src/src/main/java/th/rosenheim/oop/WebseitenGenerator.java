package th.rosenheim.oop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// Class to generate weather websites for cities.
public class WebseitenGenerator {
    private List<Stadt> staedte = new ArrayList<>();

    // Adds a city to the list.
    public void addStadt(Stadt stadt) {
        staedte.add(stadt);
    }

    // Generates the navigation bar for the website.
    public String getNavigation() {
        String stadtString = String.join("| ", staedte.stream()
                .map(Stadt -> "<a href=\"" + Stadt.getURL() + "\">" + Stadt.name + "</a>")
                .toArray(String[]::new));
        return "<h1>Die Wetter-Website</h1><p>" + stadtString + "</p>";
    }

    // Generates a full HTML page with navigation and content.
    public String generatePage(String navigation, String content) {
        return "<html><body>" + navigation + content + "</body></html>";
    }

    // Generates the HTML files for all cities and the index page.
    public void generate() {
        String navigation = getNavigation();
        try {
            // Create output directory if it doesn't exist.
            File outputDir = new File("./output");
            if (!outputDir.exists()) {
                outputDir.mkdir();
            }

            // Write the index page.
            FileWriter indexWriter = new FileWriter("./output/index.html");
            indexWriter.write(generatePage(navigation, ""));
            indexWriter.close();

            // Write the HTML file for each city.
            for (Stadt stadt : staedte) {
                FileWriter writer = new FileWriter("./output/" + stadt.getURL());
                writer.write(generatePage(navigation, stadt.getContent()));
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
