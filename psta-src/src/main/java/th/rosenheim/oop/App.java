package th.rosenheim.oop;
import java.util.List;

// Main application class to generate weather websites.
public class App {
    public static void main(String[] args) {
        WebseitenGenerator generator = new WebseitenGenerator();

        // Create instances of cities with their respective weather conditions.
        Kleinstadt rosenheim = new Kleinstadt("Rosenheim", Wetter.WOLKIG);
        Grossstadt muenchen = new Grossstadt("München", Wetter.REGNERISCH, List.of("Schwabing", "Sendling", "Lehel", "Nymphenburg"));
        Grossstadt koeln = new Grossstadt("Köln", Wetter.SONNIG, List.of("Raderthal", "Ehrenfeld", "Nippes", "Poll", "Esch", "Pesch", "Kalk"));

        // Add cities to the generator.
        generator.addStadt(rosenheim);
        generator.addStadt(muenchen);
        generator.addStadt(koeln);

        // Generate the HTML files.
        generator.generate();
    }
}
