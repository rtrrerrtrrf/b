package th.rosenheim.oop;

// Enum representing different weather conditions.
public enum Wetter {
    SONNIG("sonnig"),        // Sunny weather.
    WOLKIG("wolkig"),        // Cloudy weather.
    REGNERISCH("regnerisch");// Rainy weather.

    private final String wetter;

    // Constructor for Wetter enum, sets the weather condition.
    Wetter(String wetter) {
        this.wetter = wetter;
    }

    // Getter method to retrieve the weather condition as a string.
    public String getWetter() {
        return wetter;
    }
}
