package th.rosenheim.oop;

// Abstract class representing a city.
public abstract class Stadt {
    protected String name;
    protected Wetter wetter;

    // Constructor for Stadt, initializes name and weather.
    public Stadt(String name, Wetter wetter) {
        this.name = name;
        this.wetter = wetter;
    }

    // Formats the city name to be URL-friendly.
    protected String formatNameForURL() {
        return name.toLowerCase()
                    .replace("ä", "ae")
                    .replace("ö", "oe")
                    .replace("ü", "ue")
                    .replace("ß", "ss")
                    .replace(" ", "_");
    }

    // Abstract method to get the content for the city's webpage.
    public abstract String getContent();

    // Abstract method to get the URL for the city's webpage.
    public abstract String getURL();
}
 